/**
 * Created by a-sugita on 13/11/08.
 */
$(function(){
  //localStorage.clear();
  console.log(localStorage);
//完了タスクの数をカウント/////////////////////////////////////
    var countCompleteTaskLength = function(tasklist){
        var donetasklength=0
        for(i=0; i<=tasklist.length-1; i++){
            if(tasklist[i].doneTask()){
                donetasklength = donetasklength+1
            }
        }
        return donetasklength;
    }

    var countDownthree = function(){
        var dateNum = new Date();
        var todayYear = String(dateNum.getFullYear())
        var todayMonth = function(){
            if(dateNum.getMonth()+1<=9){
                var Month = "0"+String(dateNum.getMonth()+1)
                return Month
            }else{
                return dateNum.getMonth()+1
            }
        }
        var todayDay = function(){
            if(dateNum.getDate()<=9){
                var Day = "0"+String(dateNum.getDate())
                return Day
            }else{
                return dateNum.getDate()
            }
        }
        var todayNum = parseInt(todayYear + todayMonth() + todayDay())
        return todayNum
    }

//初期設定/////////////////////////////////////
  var taskListViewModel = function(){
    this.taskList =ko.observableArray([])//画面に表示されるタスクのリスト
    this.index = ko.observable()//表示されているタスクリストのindexが入る
    this.form = new taskEditForm(this.taskList().length)//フォームに入力した値を入れる
    this.list = new Array()//ローカルストレージにまとめて保存するためのいれもの
    this.checkError = ko.observable(false)//未入力項目フラグ
    this.dateCheckError = ko.observable(false)//存在しない日付をチェックする
    this.getTaskData = ko.observableArray([])//データを使う形に変換したときに入れとく
    this.completeLength = ko.observable()//完了したタスクの数
    this.unCompleteLength = ko.observable()//未完了のタスクの数
    this.taskStatus = ko.observable()
//////////////////////////////////////////////////////////////////////////
//一応ViewModel//////////////////////////////////////////////////////////
//更新時にローカルストレージからデータ取得/////////////////////////////////////
//取得したデータを画面に表示/////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
    var storageData = localStorage.getItem("task_collection");
    var storageDataJson = JSON.parse(storageData);
    if(storageData == null){

    }else{
        var storageLength = storageDataJson.length;
        if(storageLength>0){
          for(var i=storageLength-1; i>=0; i--){
            var combartTaskDataToObservable = this.form.combartTaskData(storageDataJson[i])
            this.taskList.unshift(combartTaskDataToObservable)
            this.list.unshift(storageDataJson[i])
          }
        }
        this.completeLength(countCompleteTaskLength(this.taskList()))
        this.unCompleteLength(this.taskList().length-this.completeLength());
    }
//    for(i=0; i<=this.taskList().length-1; i++){
//       if(countDownthree(i,this.taskList())<=3){
//           //this.taskList()[i].taskWarning(true)
//           this.taskStatus("warning_task")
//       }else if(!(this.taskList()[i].taskWarning())){
//           this.taskStatus("task_status_normal")
//       }
//
//
//    }

      this.taskStatus = function(data){
        var RemainingDays = parseInt(data.sortNum())
        if(RemainingDays-countDownthree()<=3){
          return "warning_task"
        }else{
            return "task_status_normal"
        }
      }


//ソートボタンの表示非表示/////////////////////////////////////

    if(this.taskList().length == 0){
       $(".sort_task_near").css("display", "none");
       $(".sort_task_far").css("display", "none");
    }

//タスク追加ダイアログの表示非表示/////////////////////////////////////

    this.toggleAddTaskWindow = function(){
      $(".edit_button").css("display", "none");
      $(".add_button").css("display", "inline");
      $(".addTask_form").toggle()
      $(".gray_cover").toggle()
    }
    var hideAddTaskWindow = function(){
      $(".addTask_form").css("display", "none");
      $(".gray_cover").css("display", "none");
    }

//タスク追加/////////////////////////////////////

    this.addTaskData = function(){
      if(this.taskList().length > 0){
        $(".sort_task_near").css("display", "inline");
        $(".sort_task_far").css("display", "inline");
      }
      var length = this.taskList().length
      this.list = []
      for(var i=length-1; i>=0; i--){
        var tasks = this.taskList()
        this.list.unshift(this.form.buildTaskDataJson(tasks[i]))
      }
        var nowDate = new Date()
        var nowYear = String(nowDate.getFullYear())
        var nowMonth =String(nowDate.getMonth()+1)
        var nowDay = String(nowDate.getDate())
        var sortNumDay = nowYear +"/"+ nowMonth +"/"+ nowDay;
      var formData = this.form.buildTaskData(sortNumDay)
      var check = textCheck(formData);
      if(check){
          this.dateCheckError(false)
          this.checkError(true)
      }else{
          if(dateCheck(formData)){
            this.checkError(false)
            this.dateCheckError(true)
          }else{
              var RemainingDays = parseInt(formData.sortNum())
              if(RemainingDays-countDownthree()<=3){
                  if(this.taskList().length<=0){
                      formData.taskWarning(true)
                  }else{
                    this.taskList()[0].taskWarning(true)
                  }
              }
              this.taskList.unshift(formData)
              this.list.unshift(this.form.buildTaskDataJson(formData))
              hideAddTaskWindow()
              this.form.clearTaskForm()
              AddTaskToStorage(this.list)
          }
      }
      this.completeLength(countCompleteTaskLength(this.taskList()))
      this.unCompleteLength(this.taskList().length-this.completeLength());
    }
    this.clickCancel = function(){
      hideAddTaskWindow()
    }

//タスク完了/////////////////////////////////////

    this.clickDoneTaskButton = function(data){
      var index = this.taskList.indexOf(data)
      data.doneTask( true );
      this.completeLength(countCompleteTaskLength(this.taskList()))
      this.unCompleteLength(this.taskList().length-this.completeLength());
      this.list = []
      var length = this.taskList().length
      for(var i=length-1; i>=0; i--){
          var tasks = this.taskList()
          this.list.unshift(this.form.buildTaskDataJson(tasks[i]))
      }
      AddTaskToStorage(this.list)
    }

//完了させたタスクを未完了に戻す/////////////////////////////////////

    this.clickUndoTaskButton = function(data){
      var index = this.taskList.indexOf(data)
      data.doneTask( false );
      this.completeLength(countCompleteTaskLength(this.taskList()))
      this.unCompleteLength(this.taskList().length-this.completeLength());
      this.list = []
      var length = this.taskList().length
      for(var i=length-1; i>=0; i--){
          var tasks = this.taskList()
          this.list.unshift(this.form.buildTaskDataJson(tasks[i]))
      }
      AddTaskToStorage(this.list)
    }

//タスク編集/////////////////////////////////////

    this.editTask = function(data){
      this.form.putTaskDataToForm(data)
      this.index(this.taskList.indexOf(data))
      $(".addTask_form").toggle()
      $(".gray_cover").toggle()
      $(".edit_button").css("display", "inline");
      $(".add_button").css("display", "none");

    }
    this.editSubmit = function(data){
      this.list = []
      var index = this.index()
      var formData = this.form.combartTaskData(data)
      var Data = this.form.buildTaskDataJson(formData)
      var taskListCollection = this.taskList()[index]
      taskListCollection.dateSlash(data.slash())
      taskListCollection.deadYear(data.deadLineYear())
      taskListCollection.deadMonth(data.deadLineMonth())
      taskListCollection.deadDay(data.deadLineDay())
      taskListCollection.title(data.titleName())
      //taskListCollection.assignDay(data.assign())
      var RemainingDays = parseInt(String(taskListCollection.deadYear())+String(taskListCollection.deadMonth())+String(taskListCollection.deadDay()))
        debugger
      if(RemainingDays-countDownthree()<=3){
          taskListCollection.taskWarning(true)

      }else{
          debugger
          taskListCollection.taskWarning(false)
      }
      hideAddTaskWindow()
      var length = this.taskList().length
      for(var i=length-1; i>=0; i--){
        var tasks = this.taskList()
        this.list.unshift(this.form.buildTaskDataJson(tasks[i]))
      }
      AddTaskToStorage(this.list)
      location.reload();
    }

//タスク削除/////////////////////////////////////

    this.deleteTask = function(data){
        this.index(this.taskList.indexOf(data))
        this.taskList.splice(this.index(),1)
        this.list = []
        var length = this.taskList().length
        for(var i=length-1; i>=0; i--){
            var tasks = this.taskList()
            this.list.unshift(this.form.buildTaskDataJson(tasks[i]))
        }
        AddTaskToStorage(this.list)
        this.completeLength(countCompleteTaskLength(this.taskList()))
        this.unCompleteLength(this.taskList().length-this.completeLength());
    }

//タスクソート/////////////////////////////////////

    this.sortDataNear = function(){
        this.taskList.sort(
            function(a,b){
                var aDate = a.sortNum()
                var bDate = b.sortNum()
                if( aDate < bDate ) return -1;
                if( aDate > bDate ) return 1;
                return 0;
            }
        )
    }
    this.sortDataFar = function(){
      this.taskList.sort(
            function(a,b){
                var aDate = a.sortNum()
                var bDate = b.sortNum()
                if( aDate < bDate ) return 1;
                if( aDate > bDate ) return -1;
                return 0;
            }
        )
    }

//全て完了にする/////////////////////////////////////
      this.allDone = function(){
          var donetasklength=0
          for(i=0; i<=this.taskList().length-1; i++){
              this.taskList()[i].doneTask(true)
          }
          this.list = []
          var length = this.taskList().length
          for(var i=length-1; i>=0; i--){
              var tasks = this.taskList()
              this.list.unshift(this.form.buildTaskDataJson(tasks[i]))
          }
          AddTaskToStorage(this.list)
          this.completeLength(countCompleteTaskLength(this.taskList()))
          this.unCompleteLength(this.taskList().length-this.completeLength());
      }
//全て未完了にする/////////////////////////////////////
      this.allUndo = function(){
          var donetasklength=0
          for(i=0; i<=this.taskList().length-1; i++){
              this.taskList()[i].doneTask(false)
          }
          this.list = []
          var length = this.taskList().length
          for(var i=length-1; i>=0; i--){
              var tasks = this.taskList()
              this.list.unshift(this.form.buildTaskDataJson(tasks[i]))
          }
          AddTaskToStorage(this.list)
          this.completeLength(countCompleteTaskLength(this.taskList()))
          this.unCompleteLength(this.taskList().length-this.completeLength());
      }
//全てのタスク削除/////////////////////////////////////
      this.allDelete = function(){
          this.taskList("")
          this.list = []
          AddTaskToStorage(this.list)
          this.completeLength(countCompleteTaskLength(this.taskList()))
          this.unCompleteLength(this.taskList().length-this.completeLength());
      }
  }

//////////////////////////////////////////////////////////////////////////
//一応Model/////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
  var taskEditForm = function(length){
    this.slash = ko.observable("/")
    this.yearCollection =ko.observableArray([""])
    this.monthCollection=ko.observableArray([""])
    this.dayCollection =ko.observableArray([""])

    this.deadLineYear = ko.observable("")
    this.deadLineMonth = ko.observable("")
    this.deadLineDay = ko.observable("")
    this.titleName = ko.observable("")
    this.assign = ko.observable("")
    this.sortDate = ko.observable()
    this.taskDone = ko.observable(false)
    this.taskWarning = ko.observable(false)
    this.taskNormal = ko.observable(false)

//締切日選択ボックスの中身/////////////////////////////////////

    for(i=2013; i<=2020; i++){
      this.yearCollection.push(i)
    }
    for(i=1; i<=12; i++){
      if(i<10){
          var monthNum = 0+String(i)
          parseInt(monthNum)
          i = monthNum
      }
      this.monthCollection.push(i)
    }
    for(i=1; i<=31; i++){
        if(i<10){
            var dayNum = 0+String(i)
            parseInt(dayNum)
            i = dayNum
        }
      this.dayCollection.push(i)
    }

//ローカルストレージに保存するときに、合った構造に変換。/////////////////////////////////////
    this.buildTaskDataJson = function(data){
      var JsonTaskData = new taskJsonData(data)
      return JsonTaskData
    }

//入力フォームから取得した値を画面に表示できる構造に変換/////////////////////////////////////
    this.buildTaskData = function(assignDay){
      this.getTaskData = new Task(this,assignDay)
      return this.getTaskData
    }

//ローカルストレージから取得した値を画面に表示できる構造に変換/////////////////////////////////////
    this.combartTaskData = function(data){
      this.getTaskData = new storageTask(data)
      return this.getTaskData
    }

//フォームダイアログ画面をクリア/////////////////////////////////////
    this.clearTaskForm = function(){
      this.deadLineYear("")
      this.deadLineMonth("")
      this.deadLineDay("")
      this.titleName ("")
      this.sortDate("")
    }

//editする際にフォームダイアログ画面に編集する前のデータを表示させる/////////////////////////////////////
    this.putTaskDataToForm = function(taskData){
      this.deadLineYear(taskData.deadYear())
      this.deadLineMonth(taskData.deadMonth())
      this.deadLineDay(taskData.deadDay())
      this.titleName (taskData.title())
    }
  }

//それぞれ適した配列構造に直す/////////////////////////////////////
  var Task = function(data,assignDay){
    this.dateSlash = ko.observable(data.slash())
    this.deadYear = ko.observable(data.deadLineYear())
    this.deadMonth = ko.observable(data.deadLineMonth())
    this.deadDay = ko.observable(data.deadLineDay())
    this.title = ko.observable(data.titleName())
    this.assignDay = ko.observable(assignDay)
    this.doneTask = ko.observable(data.taskDone())
    this.sortNum = ko.observable(String(data.deadLineYear())+String(data.deadLineMonth())+String(data.deadLineDay()))
    this.taskWarning = ko.observable(data.taskWarning())
    this.taskNormal = ko.observable(data.taskNormal())
  }
  var storageTask = function(data){

    this.dateSlash = ko.observable(data.slash)
    this.deadYear = ko.observable(data.deadLineYear)
    this.deadMonth = ko.observable(data.deadLineMonth)
    this.deadDay = ko.observable(data.deadLineDay)
    this.title = ko.observable(data.titleName)
    this.assignDay = ko.observable(data.assignLineDay)
    this.doneTask = ko.observable(data.line)
    this.sortNum = ko.observable(String(data.deadLineYear)+String(data.deadLineMonth)+String(data.deadLineDay))
    this.taskWarning = ko.observable(data.taskWarning)
    this.taskNormal = ko.observable(data.taskNormal)
  }

  var taskJsonData =function(data){

    var task = {
      slash:"/",
      deadLineYear:data.deadYear(),
      deadLineMonth:data.deadMonth(),
      deadLineDay:data.deadDay(),
      titleName:data.title(),
      assignLineDay:data.assignDay(),
      sortDate:String(data.deadYear())+String(data.deadMonth())+String(data.deadDay()),
      line:data.doneTask(),
      warning:data.taskWarning(),
      normal:data.taskNormal()
    }
    return task
  }

  window.viewModel = new taskListViewModel()
  ko.applyBindings(window.viewModel);

///////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//ローカルストレージに保存/////////////////////////////////////
  var AddTaskToStorage = function(tasks){
    var taskDataStr = JSON.stringify(tasks);

    // JSON 文字列を保存
    //localStorage.clear();
    localStorage.setItem("task_collection",taskDataStr);
    var local_storage = window.localStorage;
    console.log(local_storage);
  }

//ローカルストレージから駆除/////////////////////////////////////
  var removeDataStorage = function(index){
    localStorage.removeItem(localStorage.key(index));
  }

//未入力項目チェック/////////////////////////////////////
  var textCheck = function(formData){
    if(formData.deadYear() == "" || formData.deadMonth()== "" || formData.deadDay()== "" ||  formData.title()== ""){
     return true
    }
  }

//締切日存在チェック/////////////////////////////////////
  var dateCheck = function(formData){
    if(formData.deadMonth()=="02"){
        if(Number(formData.deadDay())>28){
            return true
        }
    }
    if(formData.deadMonth()=="04" || formData.deadMonth()=="06" || formData.deadMonth()=="09" || formData.deadMonth()=="11"){
        if(Number(formData.deadDay())>30){
            return true
        }
    }
  }

})


