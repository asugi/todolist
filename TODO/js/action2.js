/**
 * Created by a-sugita on 13/11/08.
 */
$(function(){
// This is a simple *viewmodel* - JavaScript that defines the data and behavior of your UI
    function AppViewModel() {
        this.name = ko.observableArray([
            {
                sample1:"test",
                sample2:"test",
                sample3:"test"
            }
        ])
        this.test1 = ko.observable()
        this.test2 = ko.observable()
        this.test3 = ko.observable()
        this.text = ko.observable("fa---")

        this.clickButton=function(){
            console.log(this)
            var testArray = {
                sample1:this.test1(),
                sample2:this.test2(),
                sample3:this.test3()
            }
            this.name.push(testArray)
            console.log(this)
        }
        this.clickButton2=function(data){
            debugger
            console.log(data)
        }

    }
// Activates knockout.js
    ko.applyBindings(new AppViewModel());
})


