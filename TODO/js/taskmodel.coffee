define [
  'action-coffee'
  'view-model'
], (
  ActionCoffee
  TaskViewModel
) ->

  class TaskModel
    constructor:() ->
      @dateSlash = ko.observable()
      @deadYear = ko.observable()
      @deadMonth = ko.observable()
      @deadDay = ko.observable()
      @title = ko.observable()
      @assignDay = ko.observable()
      @doneTask = ko.observable()
      @sortNum = ko.observable()
      @year = ko.observableArray([""])
      @month = ko.observableArray([""])
      @day = ko.observableArray([""])
      @countDownNum = ko.observable()
      @assignDate = ko.observable()
      @completeTask = ko.observable()

    builedSelect:() ->
      @year([""])
      @month([""])
      @day([""])
      for i in [2013..2020]
        @year.push(i)

      for i in [1..12]
        if i<10
          monthNum = 0+String(i)
          parseInt(monthNum)
          i = monthNum
        @month.push(i)

      for i in [1..31]
        if i<10
          dayNum = 0+String(i)
          parseInt(dayNum)
          i = dayNum
        @day.push(i)

    todayMonth:() ->
      dateNum = new Date();
      if(dateNum.getMonth()+1<=9)
        Month = "0"+String(dateNum.getMonth()+1)
        return Month
      else
        return dateNum.getMonth()+1

    todayDay:() ->
      dateNum = new Date();
      if(dateNum.getDate()<=9)
        Day = "0"+String(dateNum.getDate())
        return Day
      else
        return dateNum.getDate()

    countDownthree:() ->
      dateNum = new Date();
      todayYear = String(dateNum.getFullYear())
      todayNum = parseInt(todayYear + @todayMonth() + @todayDay())
      @countDownNum(todayNum)

    buildAssignDay:()->
      nowDate = new Date()
      nowYear = nowDate.getFullYear()
      nowMonth = nowDate.getMonth()+1
      nowDay = nowDate.getDate()
      sortNumDay = "#{nowYear} / #{nowMonth} / #{nowDay}"
      @assignDate(sortNumDay)

    loadStrageData:() ->
      loadStrageData = localStorage.getItem "task_collection"
      StrageDataToJson = JSON.parse loadStrageData
      return StrageDataToJson

    completeTaskLength:(tasklist) ->
      donetasklength = 0
      for num, i in tasklist
        if(tasklist[i].doneTask())
          donetasklength = donetasklength+1
      @completeTask(donetasklength)

    combartTaskData:(data,assign) ->
      @dateSlash("/")
      @deadYear(data.deadLineYear())
      @deadMonth(data.deadLineMonth())
      @deadDay(data.deadLineDay())
      @title(data.titleName())
      @assignDay(assign)
      @sortNum(parseInt(String(data.deadLineYear())+String(data.deadLineMonth())+String(data.deadLineDay())))
      @doneTask(false)
      return @

    combartTaskListDataFromStrage:(data,assign) ->
      @dateSlash("/")
      @deadYear(data.deadLineYear)
      @deadMonth(data.deadLineMonth)
      @deadDay(data.deadLineDay)
      @title(data.titleName)
      @assignDay(data.assignLineDay)
      @sortNum(data.sortDate)
      @doneTask(data.taskStatus)
      return @

    addTaskToStorage:(tasks) ->
      taskDataStr = JSON.stringify(tasks);
#       JSON 文字列を保存
#      localStorage.clear();
      localStorage.setItem("task_collection",taskDataStr);
      local_storage = window.localStorage;
      console.log(local_storage);




