define [
  'taskmodel'
], (
  TaskModel
) ->

  class TaskViewModel
    constructor: () ->
      @taskmodel = new TaskModel()
      @taskList =ko.observableArray([])#画面に表示されるタスクのリスト
      @list = new Array()#ローカルストレージにまとめて保存するためのいれもの
      @yearCollection =ko.observableArray([""])
      @monthCollection=ko.observableArray([""])
      @dayCollection =ko.observableArray([""])
      @slash = ko.observable("/")
      @deadLineYear = ko.observable("")
      @deadLineMonth = ko.observable("")
      @deadLineDay = ko.observable("")
      @titleName = ko.observable("")
      @assign = ko.observable("")
      @sortDate = ko.observable()
      @index = ko.observable()
      @completeLength = ko.observable()
      @unCompleteLength = ko.observable()
      @checkError = ko.observable(false)#未入力項目フラグ
      @dateCheckError = ko.observable(false)#存在しない日付をチェックする

#///更新したときの初期表示/////////////////////////////////////////////
    showHeaderButton:() ->
      if(@taskList().length>=2)
        return true


    taskStatus:(data) ->
      taskmodel = new TaskModel()
      RemainingDays = parseInt(data.sortNum())
      taskmodel.countDownthree()
      countDay = RemainingDays-taskmodel.countDownNum()
      if(countDay<=3)
        return "warning_task"
      else
        return "task_status_normal"

    firstViewData:() ->
#      localStorage.clear()
      taskModel = new TaskModel()
      debugger
      strageData = taskModel.loadStrageData()
      if !(strageData==null)
        for num, i in strageData[0] by -1
          taskModel = new TaskModel()
          combartTaskDataToObservable = taskModel.combartTaskListDataFromStrage(strageData[0][i])
          @taskList.unshift(combartTaskDataToObservable)
          @countCompleteLength()

#///呼び出される関数/////////////////////////////////////////////
    taskJsonDataSendLocalStrage:(data) ->
      sendJsonStrageData=[]
      for num, i in data
        task = {
          slash:"/",
          deadLineYear:data[i].deadYear(),
          deadLineMonth:data[i].deadMonth(),
          deadLineDay:data[i].deadDay(),
          titleName:data[i].title(),
          assignLineDay:data[i].assignDay(),
          sortDate:String(data[i].deadYear())+String(data[i].deadMonth())+String(data[i].deadDay()),
          taskStatus:data[i].doneTask()
        }
        sendJsonStrageData.push(task)
      return sendJsonStrageData

    putTaskDataToForm:(taskData) ->
      @deadLineYear(taskData.deadYear())
      @deadLineMonth(taskData.deadMonth())
      @deadLineDay(taskData.deadDay())
      @titleName (taskData.title())

    changeTaskStatus:(data,status) ->
      data.doneTask( status );
      @renewStrageTaskData()

    allChangeTaskStatus:(status) ->
      for num, i in @taskList()
        @taskList()[i].doneTask(status)
      @renewStrageTaskData()

    renewStrageTaskData:() ->
      @list = []
      @list.unshift(@taskJsonDataSendLocalStrage(@taskList()))
      @taskmodel.addTaskToStorage(@list)

    hideAddTaskWindow:() ->
      $(".addTask_form").css("display", "none");
      $(".gray_cover").css("display", "none");

    countCompleteLength:() ->
      @taskmodel.completeTaskLength(@taskList())
      @completeLength(@taskmodel.completeTask())
      @unCompleteLength(@taskList().length-@taskmodel.completeTask())

    textCheck:(formData) ->
      if(formData.deadLineYear() == "" || formData.deadLineMonth()== "" || formData.deadLineDay()== "" ||  formData.titleName()== "")
        return true

    dateCheck:(formData) ->
      if(formData.deadLineMonth()=="02")
        if(Number(formData.deadLineDay())>28)
          return true
      if(formData.deadLineMonth()=="04" || formData.deadLineMonth()=="06" || formData.deadLineMonth()=="09" || formData.deadLineMonth()=="11")
        if(Number(formData.deadLineDay())>30)
          return true

    builedSelectBox:() ->
      @taskmodel.builedSelect()
      @yearCollection(@taskmodel.year())
      @monthCollection(@taskmodel.month())
      @dayCollection(@taskmodel.day())

    sort:(retA,retB) =>
      @taskList.sort(((a,b) ->
        aDate = a.sortNum()
        bDate = b.sortNum()
        if( aDate < bDate )
          return retA
        if( aDate > bDate )
          return retB
        return 0
      ))



#///画面操作したときの処理/////////////////////////////////////////////
#///画面上部のボタン//////////////////////////////////////////////////
    toggleAddTaskWindow: () ->
#      フォームをクリア
      @deadLineYear("")
      @deadLineMonth("")
      @deadLineDay("")
      @titleName ("")
      @sortDate("")
#     締切日の選択項目作成
      @builedSelectBox()
      @checkError(false)
      @dateCheckError(false)
      $(".edit_button").css("display", "none");
      $(".add_button").css("display", "inline");
      $(".addTask_form").toggle()
      $(".gray_cover").toggle()
      return

    addTaskData: (data) ->
      @checkError(@textCheck(data))
      @dateCheckError(@dateCheck(data))
      if(!(@checkError()==true || @dateCheckError()==true))
        length = @taskList().length
        @list = []
        taskModel = new TaskModel()
        taskModel.buildAssignDay()
        addTaskData = taskModel.combartTaskData(data,taskModel.assignDate())
        @taskList.unshift(addTaskData)
        @list.unshift(@taskJsonDataSendLocalStrage(@taskList()))
        taskModel.addTaskToStorage(@list)
        @countCompleteLength()
        @hideAddTaskWindow()

    sortDataNear:() ->
      @sort(-1,1)

    sortDataFar:() ->
      @sort(1,-1)

    allDelete:() ->
      @taskList([])
      @list = []
      localStorage.clear()
      @countCompleteLength()
      if(@taskList().length<=0)
        $(".sort_task_near").css("display", "none")
        $(".sort_task_far").css("display", "none")


    allDone:() ->
      @allChangeTaskStatus(true)
      @countCompleteLength()

    allUndo:() ->
      @allChangeTaskStatus(false)
      @countCompleteLength()


#///タスク追加編集ダイアログのボタン//////////////////////////////////////////////////
    clickCancel: () ->
      @hideAddTaskWindow()

    editSubmit:(data) ->
      @checkError(@textCheck(data))
      @dateCheckError(@dateCheck(data))
      if(!(@checkError()==true || @dateCheckError()==true))
        @hideAddTaskWindow()
        @renewStrageTaskData()
        @list = []
        index = @index()
        taskModel = new TaskModel()
        @builedSelectBox()
        formData = taskModel.combartTaskListDataFromStrage(data)
        sendJsonData = @taskJsonDataSendLocalStrage(formData)
        taskListCollection = @taskList()[index]
        taskListCollection.dateSlash(data.slash())
        taskListCollection.deadYear(data.deadLineYear())
        taskListCollection.deadMonth(data.deadLineMonth())
        taskListCollection.deadDay(data.deadLineDay())
        taskListCollection.title(data.titleName())
        taskListCollection.sortNum(String(data.deadLineYear())+String(data.deadLineMonth())+String(data.deadLineDay()))
        @taskStatus(@taskList()[index])


#///各タスクのボタン//////////////////////////////////////////////////
    clickDoneTaskButton:(data) ->
      tasklist = @taskList()
      @changeTaskStatus(data,true)
      @countCompleteLength()

    clickUndoTaskButton:(data) ->
      tasklist = @taskList()
      @changeTaskStatus(data,false)
      @countCompleteLength()

    deleteTask:(data) ->
      @index(@taskList.indexOf(data))
      @taskList.splice(@index(),1)
      @renewStrageTaskData()
      @countCompleteLength()

    editTask:(data) ->
      @builedSelectBox()
      @putTaskDataToForm(data)
      @index(@taskList.indexOf(data))
      $(".addTask_form").toggle()
      $(".gray_cover").toggle()
      $(".edit_button").css("display", "inline")
      $(".add_button").css("display", "none")
